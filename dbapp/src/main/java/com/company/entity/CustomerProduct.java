package com.company.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "customer_product")
public class CustomerProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "product_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Product product;

    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Customer customer;

    public CustomerProduct() {
    }


    public CustomerProduct(Integer id) {
        this.id = id;
    }

    public CustomerProduct(Product product, Customer customer) {
        this.product = product;
        this.customer = customer;
    }

    public CustomerProduct(Integer id,Product product, Customer customer) {
        this.id = id;
        this.product = product;
        this.customer = customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "CustomerProduct{" +
                "id=" + id +
                ", product=" + product +
                ", customer=" + customer +
                '}';
    }
}
