package com.company.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @Column(name = "price")
    private Integer price;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<CustomerProduct> customerProductList;

    public Product() {
    }

    public Product(String name, Integer price, List<CustomerProduct> customerProductList) {
        this.name = name;
        this.price = price;
        this.customerProductList = customerProductList;
    }

    public Product(Integer id,String name, Integer price, List<CustomerProduct> customerProductList) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.customerProductList = customerProductList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<CustomerProduct> getCustomerProductList() {
        return customerProductList;
    }

    public void setCustomerProductList(List<CustomerProduct> customerProductList) {
        this.customerProductList = customerProductList;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
