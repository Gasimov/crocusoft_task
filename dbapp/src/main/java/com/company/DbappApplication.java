package com.company;

import com.company.dao.inter.ProductRepository;
import com.company.entity.Customer;
import com.company.dao.inter.CustomerRepository;
import com.company.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class DbappApplication {

	@Autowired
	private CustomerRepository repo;

	@Autowired
	private ProductRepository repoo;

	@Bean
	public CommandLineRunner run(){
		CommandLineRunner clr = new CommandLineRunner(){

			@Override
			public void run(String... args) throws Exception {
				List<Product> list = repoo.findAll();

				System.out.println(list);
			}
		};
		return clr;
	}



	public static void main(String[] args) {
		SpringApplication.run(DbappApplication.class, args);
	}
}
