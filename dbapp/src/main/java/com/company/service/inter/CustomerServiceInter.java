package com.company.service.inter;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface CustomerServiceInter {

     void productPurchase(int productId, int customerId);
}
