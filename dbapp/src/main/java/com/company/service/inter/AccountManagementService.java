package com.company.service.inter;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface AccountManagementService {

    boolean increaseBalance(int cId, int amount);
}
