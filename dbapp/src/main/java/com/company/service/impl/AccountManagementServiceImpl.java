package com.company.service.impl;

import com.company.dao.inter.AccountRepository;
import com.company.dao.inter.CustomerRepository;
import com.company.entity.Account;
import com.company.entity.Customer;
import com.company.service.inter.AccountManagementService;
import org.springframework.beans.factory.annotation.Autowired;

public class AccountManagementServiceImpl implements AccountManagementService {

    @Autowired
    private CustomerRepository cRepo;

    @Autowired
    private AccountRepository aRepo;

    @Override
    public boolean increaseBalance(int cId, int amount) {
        Customer customer = cRepo.getById(cId);
        int accId = customer.getAccounts().get(customer.getId()).getId();
        Account account = customer.getAccounts().get(accId);// bonus account tapdiq meselen (2 accounts of standard and bonus )

        int newBalance = account.getAmount() + amount;
        account.setAmount(newBalance);
        aRepo.save(account);
        return true;
    }
}
