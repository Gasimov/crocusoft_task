package com.company.service.impl;

import com.company.dao.inter.AccountRepository;
import com.company.dao.inter.CustomerRepository;
import com.company.dao.inter.ProductRepository;
import com.company.entity.Account;
import com.company.entity.Customer;
import com.company.entity.Product;
import com.company.service.inter.CustomerServiceInter;
import org.springframework.beans.factory.annotation.Autowired;


public class CustomerServiceImpl implements CustomerServiceInter {

    @Autowired
    private ProductRepository repo;

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private AccountRepository accountRepo;

    @Override
    public void productPurchase(int productId) {
        int customerId = 2;
        Product purchasedProd = repo.getById(productId);
        Customer customer = customerRepo.getById(customerId);
        int customerAccId = customer.getId();//hansi hesabindan aldi standard yoxsa bonus
        Account account = accountRepo.getById(customerAccId);

        int productPrice = purchasedProd.getPrice();

        if(account.getAmount() < productPrice){
            //System.out.println("no sufficient balance");
        }else {
            int newBalance = account.getAmount() - productPrice;
            account.setAmount(newBalance);
            accountRepo.save(account);
            //System.out.println("successful purchase");
        }
    }
}
