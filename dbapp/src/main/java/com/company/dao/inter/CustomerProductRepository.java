package com.company.dao.inter;


import com.company.entity.Customer;
import com.company.entity.CustomerProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface CustomerProductRepository extends JpaRepository<CustomerProduct, Integer> {

    Customer getById(int id);
}
