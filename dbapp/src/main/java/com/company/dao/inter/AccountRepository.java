package com.company.dao.inter;

import com.company.entity.Account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface AccountRepository extends JpaRepository<Account, Integer> {
}
