package com.company.controller;

import com.company.dao.inter.CustomerRepository;
import com.company.dto.CustomerDTO;
import com.company.dto.ResponseDTO;
import com.company.entity.Customer;
import com.company.service.inter.AccountManagementService;
import com.company.service.inter.CustomerServiceInter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class CustomerRestController {

    @Autowired
    private CustomerRepository dao;


    @GetMapping("/customer")
    public ResponseEntity<List> getCustomers(){
        List<Customer> list = dao.findAll();

        return ResponseEntity.ok(list);
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<ResponseDTO> getCustomer(@PathVariable("id") int id){

        Customer customer = dao.getById(id);

        CustomerDTO customerDTO = new CustomerDTO(customer);

        return ResponseEntity.ok(ResponseDTO.of(customerDTO, "customers successfully retrieved"));
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<ResponseDTO> deleteUser(@PathVariable("id") int id) {
        Customer user = dao.getById(id);
        dao.deleteById(id);
        return ResponseEntity.ok(ResponseDTO.of(new CustomerDTO(user), "a customer successfully deleted"));
    }
}
