package com.company.controller;


import com.company.dao.inter.ProductRepository;
import com.company.dto.ProductDTO;
import com.company.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductRestController {

    @Autowired
    private ProductRepository productRepo;


    @GetMapping("/products")
    public ResponseEntity<List> getProduct(){
        List<Product> list = productRepo.findAll();

        List<ProductDTO> productDTOS =new ArrayList<>();

        for(int i=0; i<list.size(); i++){
            Product product = list.get(i);
            productDTOS.add(new ProductDTO(product));
        }

        return ResponseEntity.ok(productDTOS);
    }
}
