package com.company.dto;

import com.company.entity.Customer;
import com.company.entity.CustomerProduct;
import com.company.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class CustomerDTO {

    private Integer id;
    private String name;
    private List<CustomerProductDTO> products;

    public CustomerDTO() {
    }

    public CustomerDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public CustomerDTO(Customer customer) {
        this.id =customer.getId();
        this.name =customer.getName();
        List<CustomerProductDTO> list = new ArrayList<>();

        List<CustomerProduct> customerProducts = customer.getCustomerProductList();

        for(int i=0; i<customerProducts.size(); i++){
            CustomerProduct product = customerProducts.get(i);
            list.add(new CustomerProductDTO(product));
        }
        products = list;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomerProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<CustomerProductDTO> products) {
        this.products = products;
    }
}
