package com.company.dto;

import com.company.entity.Customer;
import com.company.entity.CustomerProduct;
import com.company.entity.Product;

public class CustomerProductDTO {

    private Integer id;
    private ProductDTO product;


    public CustomerProductDTO() {
    }

    public CustomerProductDTO(CustomerProduct cproduct) {
        this.id = cproduct.getId();

        this.product = new ProductDTO(cproduct.getProduct());

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }
}
